import request from '@/utils/request'

// 查询发帖人列表
export function listPost(query) {
  return request({
    url: '/sywx/poster/list',
    method: 'get',
    params: query
  })
}

// 查询单位所有列表
export function lists(query) {
  return request({
    url: '/sywx/poster/lists',
    method: 'get',
    params: query
  })
}

// 查询发帖人详细
export function getPost(id) {
  return request({
    url: '/sywx/poster/' + id,
    method: 'get'
  })
}

// 新增发帖人
export function addPost(data) {
  return request({
    url: '/sywx/poster/',
    method: 'post',
    data: data
  })
}

// 修改发帖人
export function updatePost(data) {
  return request({
    url: '/sywx/poster/',
    method: 'put',
    data: data
  })
}

// 删除发帖人
export function delPost(id) {
  return request({
    url: '/sywx/poster/' + id,
    method: 'delete'
  })
}
