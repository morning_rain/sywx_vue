import request from '@/utils/request'

// 查询公告列表
export function list(query) {
  return request({
    url: '/sywx/yq/list',
    method: 'get',
    params: query
  })
}

// 查询公告详细
export function get(noticeId) {
  return request({
    url: '/sywx/yq/' + noticeId,
    method: 'get'
  })
}

// 新增公告
export function add(data) {
  return request({
    url: '/sywx/yq',
    method: 'post',
    data: data
  })
}

// 修改公告
export function update(data) {
  return request({
    url: '/sywx/yq',
    method: 'put',
    data: data
  })
}

// 删除公告
export function del(noticeId) {
  return request({
    url: '/sywx/yq/' + noticeId,
    method: 'delete'
  })
}

// 数据分析
export function sjfx(query) {
  return request({
    url: '/sywx/yq/sjfx',
    method: 'get',
    params: query
  })
}