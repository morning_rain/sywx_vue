import request from '@/utils/request'

// 查询单位列表
export function listPost(query) {
  return request({
    url: '/sywx/unitInfo/list',
    method: 'get',
    params: query
  })
}

// 查询单位所有列表
export function listsPost(query) {
  return request({
    url: '/sywx/unitInfo/lists',
    method: 'get',
    params: query
  })
}

// 查询单位详细
export function getPost(id) {
  return request({
    url: '/sywx/unitInfo/' + id,
    method: 'get'
  })
}

// 新增单位
export function addPost(data) {
  return request({
    url: '/sywx/unitInfo/',
    method: 'post',
    data: data
  })
}

// 修改单位
export function updatePost(data) {
  return request({
    url: '/sywx/unitInfo/',
    method: 'put',
    data: data
  })
}

// 删除单位
export function delPost(id) {
  return request({
    url: '/sywx/unitInfo/' + id,
    method: 'delete'
  })
}
